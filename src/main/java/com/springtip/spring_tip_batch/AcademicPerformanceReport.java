package com.springtip.spring_tip_batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
public class AcademicPerformanceReport {


    private final PlatformTransactionManager transactionManager;
    private final JobRepository jobRepository;
    private final JdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;

    public AcademicPerformanceReport(PlatformTransactionManager transactionManager,
                                     JobRepository jobRepository,
                                     JdbcTemplate jdbcTemplate,
                                     TransactionTemplate transactionTemplate) {
        this.transactionManager = transactionManager;
        this.jobRepository = jobRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.transactionTemplate = transactionTemplate;
    }

    @Bean
    Step academicPerformance() {
        return new StepBuilder("academic_performance_report", jobRepository)
                .tasklet((contribution, chunkContext) -> transactionTemplate.execute(status -> {

                    jdbcTemplate.execute("""
                            create table ACADEMIC_PERFORMANCE_REPORT(
                            select academic_performance,count(academic_performance  ) from STRESS_LEVEL group by academic_performance)
                            """);

                    return RepeatStatus.FINISHED;
                }), transactionManager)
                .build();
    }

}
