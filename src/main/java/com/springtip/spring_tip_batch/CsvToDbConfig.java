package com.springtip.spring_tip_batch;

import org.springframework.batch.core.*;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class CsvToDbConfig {

    private final Resource resource;
    private final JobRepository jobRepository;
    private final PlatformTransactionManager transactionManager;
    private final DataSource dataSource;

    public CsvToDbConfig(@Value("classpath:StressLevelDataset.csv") Resource resource,
                         JobRepository jobRepository,
                         PlatformTransactionManager transactionManager,
                         DataSource dataSource) {
        this.resource = resource;
        this.jobRepository = jobRepository;
        this.transactionManager = transactionManager;
        this.dataSource = dataSource;
    }

    @Bean
    public Step csvToDbStep() {
        return new StepBuilder("csvToDb", jobRepository)
                .<StressLevel, StressLevel>chunk(100, transactionManager)
                .reader(csvToDbReader())
                .writer(csvToDbWriter())
                .listener(new StepExecutionListener() {
                    @Override
                    public ExitStatus afterStep(StepExecution stepExecution) {
                        // add some logic here
                        return ExitStatus.COMPLETED;
                    }
                }).build();
    }

    @Bean
    JdbcBatchItemWriter<StressLevel> csvToDbWriter() {
        return new JdbcBatchItemWriterBuilder<StressLevel>()
                .sql(getInsertQuery)
                .dataSource(dataSource)
                .itemSqlParameterSourceProvider(this::itemSqlParameterSource)
                .build();
    }

    SqlParameterSource itemSqlParameterSource(StressLevel item) {
        var map = new HashMap<String, Object>();
        map.putAll(Map.of(
                "anxiety_level", item.anxiety_level(),
                "self_esteem", item.self_esteem(),
                "mental_health_history", item.mental_health_history(),
                "depression", item.depression(),
                "headache", item.headache(),
                "blood_pressure", item.blood_pressure(),
                "sleep_quality", item.sleep_quality(),
                "breathing_problem", item.breathing_problem(),
                "noise_level", item.noise_level(),
                "living_conditions", item.living_conditions()));
        map.putAll(Map.of(
                "safety", item.safety(),
                "basic_needs", item.basic_needs(),
                "academic_performance", item.academic_performance(),
                "study_load", item.study_load(),
                "teacher_student_relationship", item.teacher_student_relationship(),
                "future_career_concerns", item.future_career_concerns(),
                "social_support", item.social_support(),
                "peer_pressure", item.peer_pressure(),
                "extracurricular_activities", item.extracurricular_activities(),
                "bullying", item.bullying()));
        map.put("stress_level", item.stress_level());

        return new MapSqlParameterSource(map);
    }

    @Bean
    FlatFileItemReader<StressLevel> csvToDbReader() {
        return new FlatFileItemReaderBuilder<StressLevel>()
                .name("csvItemReader")
                .resource(resource)
                .delimited().delimiter(",")
                .names(getFileNames)
                .linesToSkip(1)
                .fieldSetMapper(this::mapper)
                .build();
    }

    private StressLevel mapper(FieldSet fieldSet) {
        return new StressLevel(
                fieldSet.readInt(0),
                fieldSet.readInt(1),
                fieldSet.readInt(2),
                fieldSet.readInt(3),
                fieldSet.readInt(4),
                fieldSet.readInt(5),
                fieldSet.readInt(6),
                fieldSet.readInt(7),
                fieldSet.readInt(8),
                fieldSet.readInt(9),
                fieldSet.readInt(10),
                fieldSet.readInt(11),
                fieldSet.readInt(12),
                fieldSet.readInt(13),
                fieldSet.readInt(14),
                fieldSet.readInt(15),
                fieldSet.readInt(16),
                fieldSet.readInt(17),
                fieldSet.readInt(18),
                fieldSet.readInt(19),
                fieldSet.readInt(20));
    }

    private static String getInsertQuery =
            """
                    insert into STRESS_LEVEL(
                        anxiety_level ,
                        self_esteem ,
                        mental_health_history ,
                        depression ,
                        headache ,
                        blood_pressure ,
                        sleep_quality ,
                        breathing_problem ,
                        noise_level ,
                        living_conditions ,
                        safety ,
                        basic_needs ,
                        academic_performance ,
                        study_load ,
                        teacher_student_relationship ,
                        future_career_concerns ,
                        social_support ,
                        peer_pressure ,
                        extracurricular_activities ,
                        bullying ,
                        stress_level)
                    values (
                        :anxiety_level ,
                        :self_esteem ,
                        :mental_health_history ,
                        :depression ,
                        :headache ,
                        :blood_pressure ,
                        :sleep_quality ,
                        :breathing_problem ,
                        :noise_level ,
                        :living_conditions ,
                        :safety ,
                        :basic_needs ,
                        :academic_performance ,
                        :study_load ,
                        :teacher_student_relationship ,
                        :future_career_concerns ,
                        :social_support ,
                        :peer_pressure ,
                        :extracurricular_activities ,
                        :bullying ,
                        :stress_level);
                    """;


    private final String[] getFileNames = "anxiety_level,self_esteem,mental_health_history,depression,headache,blood_pressure,sleep_quality,breathing_problem,noise_level,living_conditions,safety,basic_needs,academic_performance,study_load,teacher_student_relationship,future_career_concerns,social_support,peer_pressure,extracurricular_activities,bullying,stress_level"
            .split(",");

}