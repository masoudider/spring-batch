package com.springtip.spring_tip_batch;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.UUID;

@Component
public class Configurations {

    @Bean
    ApplicationRunner runner(JobLauncher jobLauncher, Job job) {
        return args -> {
            var parameters = new JobParametersBuilder()
                    .addString("UUID", UUID.randomUUID().toString())
                    .toJobParameters();
            var run = jobLauncher.run(job, parameters);
            var instanceId = run.getJobInstance().getInstanceId();
            System.out.println("InstanceId: " + instanceId);
        };
    }

    @Bean
    Job job(JobRepository jobRepository, Step step, CsvToDbConfig csvToDbConfig, AcademicPerformanceReport performanceReport) {
        return new JobBuilder("spring-tip-batch", jobRepository)
                .start(step)
                .next(csvToDbConfig.csvToDbStep()).on("*").stop()
                .from(csvToDbConfig.csvToDbStep()).on(ExitStatus.COMPLETED.getExitCode()).to(performanceReport.academicPerformance())
                .build().build();
    }

    @Bean
    Step step(JobRepository jobRepository, PlatformTransactionManager transactionManager, Tasklet tasklet) {
        return new StepBuilder("step_1", jobRepository)
                .tasklet(tasklet, transactionManager)
                .build();
    }

    @Bean
    @StepScope
    Tasklet tasklet(@Value("#{jobParameters['UUID']}") String uuid) {
        return (contribution, chunkContext) -> {
            System.out.println("Hello World 10, " + uuid);
            return RepeatStatus.FINISHED;
        };
    }

    @Bean
    TransactionTemplate transactionTemplate(PlatformTransactionManager transactionManager) {
        return new TransactionTemplate(transactionManager);
    }
}