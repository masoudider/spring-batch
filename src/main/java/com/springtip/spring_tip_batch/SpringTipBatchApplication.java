package com.springtip.spring_tip_batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTipBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTipBatchApplication.class, args);
	}

}
